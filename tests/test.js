const cmd = require('node-cmd');
const assert = require('assert');

describe('Test', () => {
    it('./samples/sample-1-evil-gemini.html', () => {
        cmd.get(
            'node app.js ./samples/sample-0-origin.html ./samples/sample-1-evil-gemini.html',
            function (err, data, stderr) {
                parsed_data = JSON.parse(data);
                var expected_data = "class = btn btn-success, href = #"
                assert.equal(expected_data, parsed_data['msg']);
            }
        );
    });
    it('./samples/sample-2-container-and-clone.html ', () => {
        cmd.get(
            'node app.js ./samples/sample-0-origin.html ./samples/sample-2-container-and-clone.html ',
            function (err, data, stderr) {
                parsed_data = JSON.parse(data);
                var expected_data = "class = btn test-link-ok, href = #ok, title = Make-Button, rel = next, onclick = javascript:window.okComplete(); return false;"
                assert.equal(expected_data, parsed_data['msg']);
            }
        );
    });
    it('./samples/sample-3-the-escape.html ', () => {
        cmd.get(
            'node app.js ./samples/sample-0-origin.html ./samples/sample-3-the-escape.html ',
            function (err, data, stderr) {
                parsed_data = JSON.parse(data);
                var expected_data = "class = btn btn-success, href = #ok, title = Do-Link, rel = next, onclick = javascript:window.okDone(); return false;"
                assert.equal(expected_data, parsed_data['msg']);
            }
        );
    });
    it('./samples/sample-4-the-mash.html', () => {
        cmd.get(
            'node app.js ./samples/sample-0-origin.html ./samples/sample-4-the-mash.html',
            function (err, data, stderr) {
                parsed_data = JSON.parse(data);
                var expected_data = "class = btn btn-success, onclick = javascript:location='http://google.com';, style = display:none"
                assert.equal(expected_data, parsed_data['msg']);
            }
        );
    });
});