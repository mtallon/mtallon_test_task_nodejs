const bunyan = require('bunyan');
const fs = require('fs');
const { JSDOM } = require('jsdom');

const input_args = process.argv.slice(2);
const logger = bunyan.createLogger({ name: "NodeJS Test Task" });

checkMatches = (targetElementId, original, sample) => {
    /* There are 3 different scenarios:
    Match class and href(btn btn - success and #ok)
    Match title and class (Make - button and btn btn - success)
    Match title and href(Make - button and #ok)
    */

    const originalDom = new JSDOM(original);
    const sampleDom = new JSDOM(sample);

    const originalButton = originalDom.window.document.getElementById(targetElementId);

    const nodeTags = {
        "class": originalButton.className,
        "href": originalButton.href,
        "title": originalButton.title,
        "text": originalButton.text.trim()
    }

    return sampleDom.window.document.querySelector(`*[class='${nodeTags['class']}'], *[href='${nodeTags['href']}']`) ||
        sampleDom.window.document.querySelector(`*[title='${nodeTags['title']}'], *[class='${nodeTags['class']}']`) ||
        sampleDom.window.document.querySelector(`*[title='${nodeTags['title']}'], *[href='${nodeTags['href']}']`) || null;

}

main = (original_file, sample_file) => {
    try {
        //we assume that the original file contains the id = 'make-everything-ok-button'
        const targetElementId = "make-everything-ok-button";

        const originalFile = fs.readFileSync(original_file);
        const sampleFile = fs.readFileSync(sample_file);

        var elem = checkMatches(targetElementId, originalFile, sampleFile);

        if (elem) {
            const array = Array.prototype.slice.apply(elem.attributes);
            logger.info(array.map(attr => `${attr.name} = ${attr.value}`).join(', '));
        } else {
            logger.info("New rules is needed");
        }
    } catch (err) {
        logger.error('Error trying to find element by id', err);
    }
}

if (input_args.length === 0) {
    console.log("Usage: node app.js <input_origin_file_path> <input_other_sample_file_path>");
} else {
    main(input_args[0], input_args[1]);
}

