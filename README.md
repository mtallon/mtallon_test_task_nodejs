# HTML Crawler

Analyzes HTML and finds a specific element, even after changes, using a set of extracted attributes.


### Install 

```sh
make install
```

### Test example:
```sh
make test
```

Test output:
```sh
(base) mtallon@marcelo:~/mtallon_test_task_nodejs$ make test
npm run test tests/test.js

> mtallon_test_task_nodejs@1.0.0 test /home/mtallon/mtallon_test_task_nodejs
> mocha "tests/test.js"



  Test
    ✓ ./samples/sample-1-evil-gemini.html
    ✓ ./samples/sample-2-container-and-clone.html 
    ✓ ./samples/sample-3-the-escape.html 
    ✓ ./samples/sample-4-the-mash.html


  4 passing (18ms)
```

### Usage manually example (demo files):
```sh
    node app.js ./samples/sample-0-origin.html ./samples/sample-1-evil-gemini.html
    node app.js ./samples/sample-0-origin.html ./samples/sample-2-container-and-clone.html
    node app.js ./samples/sample-0-origin.html ./samples/sample-3-the-escape.html
    node app.js ./samples/sample-0-origin.html ./samples/sample-4-the-mash.html
```


### Usage example (demo files):
```sh
make run_scripts
```

Ouput:
```sh
    (base) mtallon@marcelo:~/mtallon_test_task_nodejs$ node app.js ./samples/sample-0-origin.html ./samples/sample-1-evil-gemini.html 
        {"name":"NodeJS Test Task","hostname":"marcelo","pid":25049,"level":30,"msg":"class = btn btn-success, href = #","time":"2020-05-20T15:54:27.642Z","v":0}
    (base) mtallon@marcelo:~/mtallon_test_task_nodejs$ node app.js ./samples/sample-0-origin.html ./samples/sample-2-container-and-clone.html 
        {"name":"NodeJS Test Task","hostname":"marcelo","pid":25070,"level":30,"msg":"class = btn test-link-ok, href = #ok, title = Make-Button, rel = next, onclick = javascript:window.okComplete(); return false;","time":"2020-05-20T15:54:33.073Z","v":0}
    (base) mtallon@marcelo:~/mtallon_test_task_nodejs$ node app.js ./samples/sample-0-origin.html ./samples/sample-3-the-escape.html 
        {"name":"NodeJS Test Task","hostname":"marcelo","pid":25081,"level":30,"msg":"class = btn btn-success, href = #ok, title = Do-Link, rel = next, onclick = javascript:window.okDone(); return false;","time":"2020-05-20T15:54:36.501Z","v":0}
    (base) mtallon@marcelo:~/mtallon_test_task_nodejs$ node app.js ./samples/sample-0-origin.html ./samples/sample-4-the-mash.html 
        {"name":"NodeJS Test Task","hostname":"marcelo","pid":25092,"level":30,"msg":"class = btn btn-success, onclick = javascript:location='http://google.com';, style = display:none","time":"2020-05-20T15:54:39.968Z","v":0}
    
```